let running = false,
    mode = 'clock',
    remainingTime = 0,
    stopgo = document.getElementById('stopgo'),
    header = document.querySelector('.header'),
    toggleButton = document.querySelector('#toggleHeader'),
    colorInputs = {
        background: document.getElementById('bgcolor'),
        text: document.getElementById('txtcolor'),
    },
    duration,
    finalTime;

stopgo.disabled = true;
document.getElementById('uhrzeit').checked = true;

function showTime() {
    const dt = new Date(),
        el = document.getElementById('time');

    function padNumber(n) {
        return String(n).padStart(2, '0');
    }

    function colonColor() {
        return dt.getSeconds() % 2 === 0 ?
            colorInputs.text.value :
            colorInputs.background.value;
    }

    if (mode === 'clock') {
        el.innerHTML = `${padNumber(dt.getHours())}<span id="colon">:</span>${padNumber(dt.getMinutes())}`;
        document.getElementById('colon').style.color = colonColor();
        return;
    }

    if (running) remainingTime = (finalTime.getTime() - dt.getTime()) / 1000;

    let timeString = "";

    timeString += remainingTime < 0 ? "+" : "";
    remainingTime = remainingTime < 0 ? -remainingTime : remainingTime;

    const hours = Math.floor(remainingTime / 3600),
        minutes = Math.floor(remainingTime / 60 - hours * 60),
        seconds = Math.floor(remainingTime) % 60;

    timeString += hours ? padNumber(hours) + ":" : "";
    timeString += minutes || hours ? padNumber(minutes) : "";
    timeString += (hours || minutes ? ":" : "") + padNumber(seconds);

    el.innerHTML = timeString;
}

function setMode() {
    mode = document.querySelector("input[name='type']:checked").value;
    setDuration();
    stopgo.disabled = (mode === 'clock');
}

function startTimer() {
    if (running) return;

    duration = parseInt(document.getElementById('duration').value);
    finalTime = new Date(new Date().getTime() + duration * 60 * 1000 + 1000);
    running = true;
}

function resetTimer() {
    running = false;
    setMode();
}

function setDuration() {
    if (mode === 'timer' && !running) {
        duration = parseInt(document.getElementById('duration').value);
        remainingTime = duration * 60;
    }
}

function applyColors() {
    document.body.style.backgroundColor =
        document.getElementById('toggleHeader').style.backgroundColor =
            document.getElementById("bgcolor").value;

    document.body.style.color =
        document.getElementById('toggleHeader').style.color =
            document.getElementById("txtcolor").value;
}

document.querySelectorAll('input[type=color]')
    .forEach( el => el.addEventListener('onchange', applyColors));

document.querySelectorAll('input[name=type]')
    .forEach( el => el.addEventListener('click', setMode));

stopgo.addEventListener('click', () =>  {
    if (stopgo.value === 'Reset') resetTimer();
    else startTimer();
    stopgo.value = (stopgo.value === 'Start' ? 'Reset' : 'Start');
});

toggleButton.addEventListener('click', () => {
    header.classList.toggle('hidden');
    toggleButton.value = header.classList.contains('hidden') ? 'einblenden' : 'ausblenden';
});

setInterval(showTime, 100);
applyColors();